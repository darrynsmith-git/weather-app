import { Component, Input, ViewContainerRef } from '@angular/core';
import { WeatherService } from './app.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  online: Observable<boolean>;

  constructor(private _weatherService: WeatherService, public _notify: ToastsManager, public _view: ViewContainerRef) {
    this._notify.setRootViewContainerRef(_view);

    this.online = Observable.merge(
      Observable.of(navigator.onLine),
      Observable.fromEvent(window, 'online').mapTo(true),
      Observable.fromEvent(window, 'offline').mapTo(false)
    );

    this.online.subscribe(onlineStatus => {
      onlineStatus ? this.getWeather() : this._notify.error('You seem to be offline', 'Oops!');
    });
  }

  @Input() public loader = this._weatherService.loadingState;
  @Input() public weatherData;

  private getWeather() {
    const c: boolean = confirm('Grant me access to your location?');
    c ? this.fetchWeatherInfo() : this.sendWarning();
  }

  private fetchWeatherInfo() {
    this._weatherService.getWeatherConditionsForCurrentLocation()
      .then((climate) => {
        this.weatherData = climate;
      }).catch((error) => {
      this._notify.warning('Oops! Something went wrong', error);
    });
  }

  private sendWarning() {
    this._notify.warning('Permission not granted to fetch latest Climate Info', 'Warning');
  }
}
