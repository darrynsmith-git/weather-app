import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { environment } from '../environments/environment';

@Injectable()

export class WeatherService {

  public loadingState = { weather: false};

  constructor(private http: Http ) {}

  private getCurrentLocation() {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    });
  }

  private getWeatherConditions(location: any): Promise<any> {
    return this.http.get(environment.weatherApi.url + '?lat=' + location.coords.latitude + '&lon=' +
      location.coords.longitude + '&APPID=' + environment.weatherApi.key)
      .toPromise()
      .then(response => response.json())
  }

  public getWeatherConditionsForCurrentLocation() {
    this.loadingState.weather = true;
    return this.getCurrentLocation().then((location) => {
      return this.getWeatherConditions(location).then((weatherData) => {
        this.loadingState.weather = false;
        return weatherData;
      }).catch((error) => {
        this.loadingState.weather = false;
        return error;
      });
    }).catch((error) => {
      this.loadingState.weather = false;
      return error;
    });
  }

}
