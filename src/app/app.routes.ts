import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

export const router: Routes = [
  { path: '', component: AppComponent }
]

export const RoutesModule: ModuleWithProviders = RouterModule.forRoot(router);
