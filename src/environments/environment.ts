// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  weatherApi : {
    key : '0a846cba0c6894a5d859b11ea23564d7',
    url : 'http://api.openweathermap.org/data/2.5/weather'
  }
};
